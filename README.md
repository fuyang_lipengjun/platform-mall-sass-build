# platform-mall-sass-build

## 官网
<https://fly2you.cn>

## 文档
<http://doc.fly2you.cn>

## SaaS后台管理系统
<http://saas.fly2you.cn/#/login>

## 目录结构
本项目为SaaS版打包后的源码，下面是整个项目的目录结构。
```bash
├── h5                      // 公众号、H5端页面
└── mp-weixin               // 微信小程序端
```

## 部署步骤
1. 替换domainId，在本项目全局搜索`8558a9d49fb2bcbb8ef44477aaea7d3f`，修改为自己的domainId。（domainId在后台管理系统查看）

### 公众号、H5端页面部署
2. 拷贝`h5`目录到服务器，路径这里为`/home/www/h5`
3. 配置nginx
```
location /h5 {
        alias '/home/www/h5';
        expires   7d;
}
```
重启nginx
```bash
cd /usr/local/nginx/sbin
./nginx -s reload
```

### 微信小程序端部署
2. 打开微信开发者工具，导入项目，选择`mp-weixin`目录。
3. 修改项目AppId为自己申请的。
4. 上传到微信公众平台。
5. 登录微信公众平台，打开菜单`开发 -> 开发管理 -> 开发设置`
   1. 设置request合法域名`https://apis.map.qq.com`、`https://saas.fly2you.cn`
   2. 设置socket合法域名`https://saas.fly2you.cn`
   3. 设置downloadFile合法域名`https://saas.fly2you.cn`、`https://thirdwx.qlogo.cn`、`https://wx.qlogo.cn`、还有你自己设置的第三方存储平台域名
   4. 到版本管理提交审核即可。

## 注意事项
- 公众号、H5端页面部署目录必须使用h5，请勿修改。
- 如需修改公司等水印请全局搜索`微同`进行替换。
- 如果小程序还未开通直播，删除`/mp-weixin/app.json`文件中的插件`live-player-plugin`
- 项目合作洽谈，请联系客服微信（使用微信扫码添加好友，请注明来意）。
- 如需了解更多请联系客服。<br>
   ![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/wx.png "微信")
